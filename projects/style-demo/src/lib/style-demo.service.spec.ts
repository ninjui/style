import { TestBed } from '@angular/core/testing';

import { StyleDemoService } from './style-demo.service';

describe('StyleDemoService', () => {
  let service: StyleDemoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StyleDemoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
