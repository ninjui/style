import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo',
  templateUrl: './style-demo.component.html',
  styleUrls: ['./style-demo.component.scss']
})
export class StyleDemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
