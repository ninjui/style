// @ts-nocheck

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemoLayoutComponent } from './demo-layout.component';

const routes: Routes = [
  { path: '', component: DemoLayoutComponent},
  { path: 'container', loadChildren: () => import('./container/demo-container.module').then(m => m.DemoContainerModule) },
  { path: 'cosmetic', loadChildren: () => import('./cosmetic/demo-cosmetic.module').then(m => m.DemoCosmeticModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoLayoutRoutingModule {}
