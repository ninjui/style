import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoLayoutRoutingModule } from './demo-layout-routing.module';
import { DemoLayoutComponent } from './demo-layout.component';


@NgModule({
  declarations: [DemoLayoutComponent],
  imports: [
    CommonModule,
    DemoLayoutRoutingModule
  ]
})
export class DemoLayoutModule { }
