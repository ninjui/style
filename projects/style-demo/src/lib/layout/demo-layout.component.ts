import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-layout',
  templateUrl: './demo-layout.component.html',
  styleUrls: ['./demo-layout.component.scss']
})
export class DemoLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
