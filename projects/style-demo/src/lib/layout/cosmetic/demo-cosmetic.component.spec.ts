import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoCosmeticComponent } from './demo-cosmetic.component';

describe('DemoCosmeticComponent', () => {
  let component: DemoCosmeticComponent;
  let fixture: ComponentFixture<DemoCosmeticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoCosmeticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoCosmeticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
