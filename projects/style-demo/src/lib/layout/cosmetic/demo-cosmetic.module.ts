import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoCosmeticRoutingModule } from './demo-cosmetic-routing.module';
import { DemoCosmeticComponent } from './demo-cosmetic.component';


@NgModule({
  declarations: [DemoCosmeticComponent],
  imports: [
    CommonModule,
    DemoCosmeticRoutingModule
  ]
})
export class DemoCosmeticModule { }
