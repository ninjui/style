import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoCosmeticComponent } from './demo-cosmetic.component';

const routes: Routes = [{ path: '', component: DemoCosmeticComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoCosmeticRoutingModule { }
