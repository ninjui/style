import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-container',
  templateUrl: './demo-container.component.html',
  styleUrls: ['./demo-container.component.scss']
})
export class DemoContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
