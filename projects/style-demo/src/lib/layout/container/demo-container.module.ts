import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoContainerRoutingModule } from './demo-container-routing.module';
import { DemoContainerComponent } from './demo-container.component';


@NgModule({
  declarations: [DemoContainerComponent],
  imports: [
    CommonModule,
    DemoContainerRoutingModule
  ]
})
export class DemoContainerModule { }
