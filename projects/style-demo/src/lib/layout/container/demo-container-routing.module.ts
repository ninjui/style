import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoContainerComponent } from './demo-container.component';

const routes: Routes = [{ path: '', component: DemoContainerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoContainerRoutingModule { }
