// @ts-nocheck

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoUtilitiesComponent } from './demo-utilities.component';

const routes: Routes = [
  { path: '', component: DemoUtilitiesComponent },
  { path: 'display', loadChildren: () => import('./display/demo-display.module').then(m => m.DemoDisplayModule) },
  { path: 'breakpoints', loadChildren: () => import('./breakpoints/demo-breakpoints.module').then(m => m.DemoBreakpointsModule) },
  { path: 'flex', loadChildren: () => import('./flex/demo-flex.module').then(m => m.DemoFlexModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUtilitiesRoutingModule { }
