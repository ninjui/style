import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-utilities',
  templateUrl: './demo-utilities.component.html',
  styleUrls: ['./demo-utilities.component.scss']
})
export class DemoUtilitiesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
