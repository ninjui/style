import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoBreakpointsRoutingModule } from './demo-breakpoints-routing.module';
import { DemoBreakpointsComponent } from './demo-breakpoints.component';


@NgModule({
  declarations: [DemoBreakpointsComponent],
  imports: [
    CommonModule,
    DemoBreakpointsRoutingModule
  ]
})
export class DemoBreakpointsModule { }
