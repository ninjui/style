import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoBreakpointsComponent } from './demo-breakpoints.component';

describe('DemoBreakpointsComponent', () => {
  let component: DemoBreakpointsComponent;
  let fixture: ComponentFixture<DemoBreakpointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoBreakpointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoBreakpointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
