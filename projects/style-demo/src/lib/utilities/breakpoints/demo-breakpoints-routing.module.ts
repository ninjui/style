import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoBreakpointsComponent } from './demo-breakpoints.component';

const routes: Routes = [{ path: '', component: DemoBreakpointsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoBreakpointsRoutingModule { }
