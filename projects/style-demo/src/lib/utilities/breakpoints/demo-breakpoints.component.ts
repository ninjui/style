import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-breakpoints',
  templateUrl: './demo-breakpoints.component.html',
  styleUrls: ['./demo-breakpoints.component.scss']
})
export class DemoBreakpointsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
