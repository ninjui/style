import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoUtilitiesComponent } from './demo-utilities.component';

describe('DemoUtilitiesComponent', () => {
  let component: DemoUtilitiesComponent;
  let fixture: ComponentFixture<DemoUtilitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoUtilitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoUtilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
