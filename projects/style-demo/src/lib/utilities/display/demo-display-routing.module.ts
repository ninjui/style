import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoDisplayComponent } from './demo-display.component';

const routes: Routes = [{ path: '', component: DemoDisplayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoDisplayRoutingModule { }
