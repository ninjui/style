import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoDisplayRoutingModule } from './demo-display-routing.module';
import { DemoDisplayComponent } from './demo-display.component';


@NgModule({
  declarations: [DemoDisplayComponent],
  imports: [
    CommonModule,
    DemoDisplayRoutingModule
  ]
})
export class DemoDisplayModule { }
