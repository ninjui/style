import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoFlexComponent } from './demo-flex.component';

const routes: Routes = [{ path: '', component: DemoFlexComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoFlexRoutingModule { }
