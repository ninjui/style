import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoFlexRoutingModule } from './demo-flex-routing.module';
import { DemoFlexComponent } from './demo-flex.component';


@NgModule({
  declarations: [DemoFlexComponent],
  imports: [
    CommonModule,
    DemoFlexRoutingModule
  ]
})
export class DemoFlexModule { }
