import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoFlexComponent } from './demo-flex.component';

describe('DemoFlexComponent', () => {
  let component: DemoFlexComponent;
  let fixture: ComponentFixture<DemoFlexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoFlexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoFlexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
