import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-flex',
  templateUrl: './demo-flex.component.html',
  styleUrls: ['./demo-flex.component.scss']
})
export class DemoFlexComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
