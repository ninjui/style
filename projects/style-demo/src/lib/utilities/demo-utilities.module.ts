import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUtilitiesRoutingModule } from './demo-utilities-routing.module';
import { DemoUtilitiesComponent } from './demo-utilities.component';


@NgModule({
  declarations: [DemoUtilitiesComponent],
  imports: [
    CommonModule,
    DemoUtilitiesRoutingModule
  ]
})
export class DemoUtilitiesModule { }
