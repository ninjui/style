export interface ScssVar {
  group?: string;
  name?: string;
  description?: string;
  default?: string;
  scss?: boolean;
  css?: boolean;
}

const scssRegex = /(?:^\/\/ (.*?)$\n)?^\$nui-(.*?): +(.*?) !default;$|^\/\/-- (.*?)$/gm;
const cssRegex = /(?:\/\/ (.*?)$\n)?^ {4}--nui-(.*?): +(.*?);$|\/\/-- (.*?)$/gm;

let vars: {[key: string]: ScssVar[]} = {};
let lookup = {};
let group = 'Variables';
const addVar = (v) => {
  if (lookup[v.name]) {
    v = Object.assign(lookup[v.name], v);
  } else {
    lookup[v.name] = v;

    const grp = group.toLowerCase();
    vars[grp] = vars[grp] || [];
    vars[grp].push(v);
  }

  if (!v.group) {
    v.group = group;
  }
};

export const processScssFile = (scss: string) => {
  const scssVars = /^([^@import].*?)@mixin/sm.exec(scss)[1];
  const cssVars = /^  :root {\n(.*?)}$/sm.exec(scss)[1];

  vars = {};
  parseScssVars(scssVars);
  parseCssVars(cssVars);

  lookup = {};
  return vars;
};

const parseScssVars = (str) => {
  let m;
  group = 'Variables';

// tslint:disable-next-line:no-conditional-assignment
  while ((m = scssRegex.exec(str)) !== null) {
    if (m.index === scssRegex.lastIndex) {
      scssRegex.lastIndex++;
    }

    if (m[1]) {
      addVar({
        description: m[1],
        name: m[2],
        default: m[3],
        scss: true
      });
    } else {
      group = m[4];
    }
  }
};

const parseCssVars = (str) => {
  let m;
  group = 'Internal';

// tslint:disable-next-line:no-conditional-assignment
  while ((m = cssRegex.exec(str)) !== null) {
    if (m.index === cssRegex.lastIndex) {
      cssRegex.lastIndex++;
    }

    if (m[4]) {
      group = m[4];
    } else {
      const obj: ScssVar = {
        name: m[2],
        css: true
      };

      if (!m[3].startsWith('#{$')) {
        obj.name = m[2];
        obj.default = m[3];

        if (m[1]) {
          obj.description = m[1];
        }
      }

      addVar(obj);
    }
  }
};
