import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StyleDemoComponent } from './style-demo.component';

export const styleDemoRoutes: Routes = [
  { path: '', component: StyleDemoComponent },
  { path: 'core', loadChildren: () => import('./core/demo-core.module').then(m => m.DemoCoreModule) },
  { path: 'typography', loadChildren: () => import('./typography/demo-typography.module').then(m => m.DemoTypographyModule) },
  { path: 'layout', loadChildren: () => import('./layout/demo-layout.module').then(m => m.DemoLayoutModule) },
  { path: 'buttons', loadChildren: () => import('./buttons/demo-buttons.module').then(m => m.DemoButtonsModule) },
  { path: 'utilities', loadChildren: () => import('./utilities/demo-utilities.module').then(m => m.DemoUtilitiesModule) },
  { path: 'forms', loadChildren: () => import('./forms/demo-forms.module').then(m => m.DemoFormsModule) },
  { path: 'components', loadChildren: () => import('./components/demo-components.module').then(m => m.DemoComponentsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(styleDemoRoutes)],
  exports: [RouterModule]
})
export class StyleDemoRoutingModule { }
