import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoFormsRoutingModule } from './demo-forms-routing.module';
import { DemoFormsComponent } from './demo-forms.component';


@NgModule({
  declarations: [DemoFormsComponent],
  imports: [
    CommonModule,
    DemoFormsRoutingModule
  ]
})
export class DemoFormsModule { }
