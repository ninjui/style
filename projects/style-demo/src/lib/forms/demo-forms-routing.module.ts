// @ts-nocheck

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoFormsComponent } from './demo-forms.component';

const routes: Routes = [
  { path: '', component: DemoFormsComponent },
  { path: 'control-wrapper', loadChildren: () => import('./control-wrapper/demo-control-wrapper.module').then(m => m.DemoControlWrapperModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoFormsRoutingModule { }
