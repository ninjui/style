import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoControlWrapperRoutingModule } from './demo-control-wrapper-routing.module';
import { DemoControlWrapperComponent } from './demo-control-wrapper.component';


@NgModule({
  declarations: [DemoControlWrapperComponent],
  imports: [
    CommonModule,
    DemoControlWrapperRoutingModule
  ]
})
export class DemoControlWrapperModule { }
