import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoControlWrapperComponent } from './demo-control-wrapper.component';

describe('DemoControlWrapperComponent', () => {
  let component: DemoControlWrapperComponent;
  let fixture: ComponentFixture<DemoControlWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoControlWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoControlWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
