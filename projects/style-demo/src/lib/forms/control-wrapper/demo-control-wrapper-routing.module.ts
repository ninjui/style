import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoControlWrapperComponent } from './demo-control-wrapper.component';

const routes: Routes = [{ path: '', component: DemoControlWrapperComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoControlWrapperRoutingModule { }
