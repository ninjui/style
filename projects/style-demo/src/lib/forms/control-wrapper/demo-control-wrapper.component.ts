import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-control-wrapper',
  templateUrl: './demo-control-wrapper.component.html',
  styleUrls: ['./demo-control-wrapper.component.scss']
})
export class DemoControlWrapperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
