import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-intro-header',
  templateUrl: './intro-header.component.html',
  styleUrls: ['./intro-header.component.scss']
})
export class IntroHeaderComponent implements OnInit {
  @Input() name: string;
  @Input() description: string;

  constructor() { }

  ngOnInit(): void {
  }

}
