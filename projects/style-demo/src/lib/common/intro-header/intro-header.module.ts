import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroHeaderComponent } from './intro-header.component';



@NgModule({
  declarations: [IntroHeaderComponent],
  imports: [
    CommonModule
  ],
  exports: [IntroHeaderComponent]
})
export class IntroHeaderModule { }
