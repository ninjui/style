import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubnavComponent } from './subnav.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [SubnavComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [SubnavComponent]
})
export class SubnavModule { }
