import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: '[nui-style-demo-subnav]',
  templateUrl: './subnav.component.html',
  styleUrls: ['./subnav.component.scss']
})
export class SubnavComponent implements OnInit {
  @HostBinding('class.nui-list-tabs') clz = true;

  constructor() { }

  ngOnInit(): void {
  }
}


