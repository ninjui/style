// @ts-nocheck

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoComponentsComponent } from './demo-components.component';

const routes: Routes = [
  { path: '', component: DemoComponentsComponent },
  { path: 'sidebar', loadChildren: () => import('./sidebar/demo-sidebar.module').then(m => m.DemoSidebarModule) },
  { path: 'tabs', loadChildren: () => import('./tabs/demo-tabs.module').then(m => m.DemoTabsModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoComponentsRoutingModule { }
