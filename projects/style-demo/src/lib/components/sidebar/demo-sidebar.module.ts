import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarRoutingModule } from './demo-sidebar-routing.module';
import { DemoSidebarComponent } from './demo-sidebar.component';
import { FormsModule } from '@angular/forms';
import { IntroHeaderModule } from '../../common/intro-header/intro-header.module';
import { SubnavModule } from '../../common/subnav/subnav.module';


@NgModule({
  declarations: [DemoSidebarComponent],
  imports: [
    CommonModule,
    DemoSidebarRoutingModule,
    FormsModule,
    IntroHeaderModule,
    SubnavModule
  ]
})
export class DemoSidebarModule { }
