import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'nui-style-demo-sidebar',
  templateUrl: './demo-sidebar.component.html',
  styleUrls: ['./demo-sidebar.component.scss']
})
export class DemoSidebarComponent implements OnInit {
  openClose = true;
  headerMenuIdx = 0;
  subheader: string;
  header: string;

  constructor(private route: Router) {
  }

  ngOnInit(): void {
    this.getSubheader();
    this.route.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe(e => this.getSubheader());

    this.openCloseRoutine();
  }

  getSubheader() {
    this.subheader = /\/([^\/]*?)$/.exec(this.route.url)[1];
    if (this.subheader === 'overview') {
      this.header = 'Sidebars';
    } else {
      this.header = `Sidebar ${this.subheader}`;
    }
  }

  openCloseRoutine() {
    this.openClose = !this.openClose;

    if (this.openClose) {
      setTimeout(() => this.sidenavPlay(), 2000);
      setTimeout(() => this.openCloseRoutine(), 9000);
    } else {
      setTimeout(() => this.openCloseRoutine(), 3000);
    }
  }

  sidenavPlay() {
    if (this.openClose) {
      this.headerMenuIdx = this.headerMenuIdx < 3 ? this.headerMenuIdx += 1 : this.headerMenuIdx = 0;
      setTimeout(() => this.sidenavPlay(), 2000);
    }
  }
}
