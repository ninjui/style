import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DemoSidebarComponent } from './demo-sidebar.component';

const routes: Routes = [
  {
    path: '', component: DemoSidebarComponent, children: [
      {path: '', redirectTo: 'overview' },
      {path: 'overview', loadChildren: () => import('./overview/demo-sidebar-overview.module').then(m => m.DemoSidebarOverviewModule)},
      {path: 'variables', loadChildren: () => import('./vars/demo-sidebar-vars.module').then(m => m.DemoSidebarVarsModule)},
      {path: 'dojo', loadChildren: () => import('./designer/demo-sidebar-designer.module').then(m => m.DemoSidebarDesignerModule)},
      {path: 'examples', loadChildren: () => import('./examples/demo-sidebar-examples.module').then(m => m.DemoSidebarExamplesModule)},
      {path: 'breakdown', loadChildren: () => import('./breakdown/demo-sidebar-breakdown.module').then(m => m.DemoSidebarBreakdownModule)}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSidebarRoutingModule {}
