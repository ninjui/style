import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidebarOverviewComponent } from './demo-sidebar-overview.component';

describe('DemoSidebarOverviewComponent', () => {
  let component: DemoSidebarOverviewComponent;
  let fixture: ComponentFixture<DemoSidebarOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidebarOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidebarOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
