import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoSidebarOverviewComponent } from './demo-sidebar-overview.component';

const routes: Routes = [{ path: '', component: DemoSidebarOverviewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSidebarOverviewRoutingModule { }
