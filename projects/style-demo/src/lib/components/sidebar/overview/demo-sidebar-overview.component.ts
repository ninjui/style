import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-sidebar-overview',
  templateUrl: './demo-sidebar-overview.component.html',
  styleUrls: ['./demo-sidebar-overview.component.scss']
})
export class DemoSidebarOverviewComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }
}
