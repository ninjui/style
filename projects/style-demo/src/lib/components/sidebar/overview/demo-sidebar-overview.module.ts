import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarOverviewRoutingModule } from './demo-sidebar-overview-routing.module';
import { DemoSidebarOverviewComponent } from './demo-sidebar-overview.component';
import { IntroHeaderModule } from '../../../common/intro-header/intro-header.module';
import { SubnavModule } from '../../../common/subnav/subnav.module';


@NgModule({
  declarations: [DemoSidebarOverviewComponent],
  imports: [
    CommonModule,
    DemoSidebarOverviewRoutingModule,
    IntroHeaderModule,
    SubnavModule
  ]
})
export class DemoSidebarOverviewModule { }
