import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidebarDesignerComponent } from './demo-sidebar-designer.component';

describe('DemoSidebarDesignerComponent', () => {
  let component: DemoSidebarDesignerComponent;
  let fixture: ComponentFixture<DemoSidebarDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidebarDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidebarDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
