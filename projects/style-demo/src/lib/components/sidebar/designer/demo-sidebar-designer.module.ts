import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarDesignerRoutingModule } from './demo-sidebar-designer-routing.module';
import { DemoSidebarDesignerComponent } from './demo-sidebar-designer.component';
import { SubnavModule } from '../../../common/subnav/subnav.module';


@NgModule({
  declarations: [DemoSidebarDesignerComponent],
  imports: [
    CommonModule,
    DemoSidebarDesignerRoutingModule,
    SubnavModule
  ]
})
export class DemoSidebarDesignerModule { }
