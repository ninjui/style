import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoSidebarDesignerComponent } from './demo-sidebar-designer.component';

const routes: Routes = [{ path: '', component: DemoSidebarDesignerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSidebarDesignerRoutingModule { }
