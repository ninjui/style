import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidebarBreakdownComponent } from './demo-sidebar-breakdown.component';

describe('DemoSidebarBreakdownComponent', () => {
  let component: DemoSidebarBreakdownComponent;
  let fixture: ComponentFixture<DemoSidebarBreakdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidebarBreakdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidebarBreakdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
