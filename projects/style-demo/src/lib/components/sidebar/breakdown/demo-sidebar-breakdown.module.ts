import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarBreakdownRoutingModule } from './demo-sidebar-breakdown-routing.module';
import { DemoSidebarBreakdownComponent } from './demo-sidebar-breakdown.component';
import { SubnavModule } from '../../../common/subnav/subnav.module';


@NgModule({
  declarations: [DemoSidebarBreakdownComponent],
  imports: [
    CommonModule,
    DemoSidebarBreakdownRoutingModule,
    SubnavModule
  ]
})
export class DemoSidebarBreakdownModule { }
