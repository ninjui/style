import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoSidebarBreakdownComponent } from './demo-sidebar-breakdown.component';

const routes: Routes = [{ path: '', component: DemoSidebarBreakdownComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSidebarBreakdownRoutingModule { }
