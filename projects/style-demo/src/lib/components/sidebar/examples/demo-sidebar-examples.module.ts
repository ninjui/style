import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarExamplesRoutingModule } from './demo-sidebar-examples-routing.module';
import { DemoSidebarExamplesComponent } from './demo-sidebar-examples.component';
import { SubnavModule } from '../../../common/subnav/subnav.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [DemoSidebarExamplesComponent],
  imports: [
    CommonModule,
    DemoSidebarExamplesRoutingModule,
    SubnavModule,
    FormsModule
  ]
})
export class DemoSidebarExamplesModule { }
