import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidebarExamplesComponent } from './demo-sidebar-examples.component';

describe('DemoSidebarExamplesComponent', () => {
  let component: DemoSidebarExamplesComponent;
  let fixture: ComponentFixture<DemoSidebarExamplesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidebarExamplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidebarExamplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
