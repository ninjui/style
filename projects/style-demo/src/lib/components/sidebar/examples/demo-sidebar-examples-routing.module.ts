import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoSidebarExamplesComponent } from './demo-sidebar-examples.component';

const routes: Routes = [{ path: '', component: DemoSidebarExamplesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoSidebarExamplesRoutingModule { }
