import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-sidebar-examples',
  templateUrl: './demo-sidebar-examples.component.html',
  styleUrls: ['./demo-sidebar-examples.component.scss']
})
export class DemoSidebarExamplesComponent implements OnInit {
  right = false;
  push = false;
  open = false;

  nativeExample = `<input id="native" class="nui-ctrl" type="checkbox">
<div class="nui-sidebar-expand">
  <aside>
    <label for="native" style="padding:16px">🍔</label>
  </aside>
  <div>Example Content</div>
</div>`;

  listExample = `<div class="nui-sidebar-expand">
  <aside>
    <div>
      <input id="eItem_1" type="checkbox" class="nui-ctrl">
      <div class="nui-list-item">
        <div>
          <span class="nui-list-item-icon"><span>📄</span></span>
          <span>Item 1</span>
        </div>
        <label class="nui-list-item-toggle" for="eItem_1"><span>▶</span></label>
      </div>

      <!-- Enable animation by calculating the child-height based on knowing how many children -->
      <div class="nui-list-children" style="--nui-list-item-child-height: calc(var(--nui-list-item-height) * 1)">
        <a class="nui-list-item">
          <div>
            <span class="nui-list-item-icon"><span>👽</span></span>
            <span>Item 1 - Child 1</span>
          </div>
        </a>
      </div>
    </div>
  </aside>
  Test Content
</div>`;

  constructor() { }

  ngOnInit(): void {
  }

  getExample(type = '', content = '<div>Screen Content</div>', indent = '') {
    return `<div class="nui-sidebar${this.right ? '-right' : ''}${type ? '-' + type : ''}${this.push ? ' nui-push' : ''}${this.open ? ' nui-open' : ''}">
${indent}  <aside>Sidebar Content</aside>
${indent}  ${content}
${indent}</div>`;
  }
}
