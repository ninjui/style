import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoSidebarVarsRoutingModule } from './demo-sidebar-vars-routing.module';
import { DemoSidebarVarsComponent } from './demo-sidebar-vars.component';
import { SubnavModule } from '../../../common/subnav/subnav.module';


@NgModule({
  declarations: [DemoSidebarVarsComponent],
  imports: [
    CommonModule,
    DemoSidebarVarsRoutingModule,
    SubnavModule
  ]
})
export class DemoSidebarVarsModule { }
