import { Component, OnInit } from '@angular/core';
import { processScssFile } from '../../../utils';
const scss = require('!raw-loader!projects/style/component/sidebar.scss').default;

@Component({
  selector: 'nui-style-demo-sidebar-vars',
  templateUrl: './demo-sidebar-vars.component.html',
  styleUrls: ['./demo-sidebar-vars.component.scss']
})
export class DemoSidebarVarsComponent implements OnInit {
  scss = processScssFile(scss);

  constructor() { }

  ngOnInit(): void {
  }

}
