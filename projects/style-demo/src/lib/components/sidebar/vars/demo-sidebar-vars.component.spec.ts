import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSidebarVarsComponent } from './demo-sidebar-vars.component';

describe('DemoSidebarVarsComponent', () => {
  let component: DemoSidebarVarsComponent;
  let fixture: ComponentFixture<DemoSidebarVarsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoSidebarVarsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoSidebarVarsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
