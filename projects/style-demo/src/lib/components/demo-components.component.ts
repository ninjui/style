import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-components',
  templateUrl: './demo-components.component.html',
  styleUrls: ['./demo-components.component.scss']
})
export class DemoComponentsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
