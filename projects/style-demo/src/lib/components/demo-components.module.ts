import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoComponentsRoutingModule } from './demo-components-routing.module';
import { DemoComponentsComponent } from './demo-components.component';


@NgModule({
  declarations: [DemoComponentsComponent],
  imports: [
    CommonModule,
    DemoComponentsRoutingModule
  ]
})
export class DemoComponentsModule { }
