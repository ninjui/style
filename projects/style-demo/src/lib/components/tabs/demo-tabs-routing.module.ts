import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoTabsComponent } from './demo-tabs.component';

const routes: Routes = [{ path: '', component: DemoTabsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoTabsRoutingModule { }
