import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoTabsRoutingModule } from './demo-tabs-routing.module';
import { DemoTabsComponent } from './demo-tabs.component';


@NgModule({
  declarations: [DemoTabsComponent],
  imports: [
    CommonModule,
    DemoTabsRoutingModule
  ]
})
export class DemoTabsModule { }
