import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nui-style-demo-tabs',
  templateUrl: './demo-tabs.component.html',
  styleUrls: ['./demo-tabs.component.scss']
})
export class DemoTabsComponent implements OnInit {
  tabIdx = 1;

  constructor() { }

  ngOnInit(): void {
  }

}
