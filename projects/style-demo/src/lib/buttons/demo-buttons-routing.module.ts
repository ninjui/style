import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoButtonsComponent } from './demo-buttons.component';

const routes: Routes = [{ path: '', component: DemoButtonsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoButtonsRoutingModule { }
