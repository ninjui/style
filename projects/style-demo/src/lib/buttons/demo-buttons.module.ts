import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoButtonsRoutingModule } from './demo-buttons-routing.module';
import { DemoButtonsComponent } from './demo-buttons.component';


@NgModule({
  declarations: [DemoButtonsComponent],
  imports: [
    CommonModule,
    DemoButtonsRoutingModule
  ]
})
export class DemoButtonsModule { }
