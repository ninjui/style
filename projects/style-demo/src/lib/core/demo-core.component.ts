import { Component, OnInit } from '@angular/core';

@Component({
  selector:    'nui-style-demo-nui-demo-core',
  templateUrl: './demo-core.component.html',
  styleUrls:   ['./demo-core.component.scss']
})
export class DemoCoreComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
