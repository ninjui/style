import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoCoreRoutingModule } from './demo-core-routing.module';
import { DemoCoreComponent } from './demo-core.component';


@NgModule({
  declarations: [DemoCoreComponent],
  imports: [
    CommonModule,
    DemoCoreRoutingModule
  ]
})
export class DemoCoreModule { }
