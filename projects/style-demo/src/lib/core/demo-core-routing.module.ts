import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoCoreComponent } from './demo-core.component';

const routes: Routes = [{ path: '', component: DemoCoreComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoCoreRoutingModule { }
