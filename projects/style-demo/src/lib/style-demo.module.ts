import { NgModule } from '@angular/core';
import { StyleDemoComponent } from './style-demo.component';
import { StyleDemoRoutingModule } from './style-demo-routing.module';

@NgModule({
  declarations: [StyleDemoComponent],
  imports: [
    StyleDemoRoutingModule
  ],
  exports: [StyleDemoComponent]
})
export class StyleDemoModule { }
