import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoTypographyRoutingModule } from './demo-typography-routing.module';
import { DemoTypographyComponent } from './demo-typography.component';


@NgModule({
  declarations: [DemoTypographyComponent],
  imports: [
    CommonModule,
    DemoTypographyRoutingModule
  ]
})
export class DemoTypographyModule { }
