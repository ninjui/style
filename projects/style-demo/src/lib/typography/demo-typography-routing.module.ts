import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoTypographyComponent } from './demo-typography.component';

const routes: Routes = [{ path: '', component: DemoTypographyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoTypographyRoutingModule { }
