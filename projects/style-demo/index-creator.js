const fs = require('fs');
const path = require('path');
const libPath = path.join(__dirname, 'src', 'lib');
const fileTest = f => f.endsWith('.module.ts') && !f.endsWith('-routing.module.ts') || f.endsWith('.component.ts') || f.endsWith('.service.ts');

const walk = function (dir, done) {
  let results = [];

  fs.readdir(dir, function (err, list) {
    if (err) return done(err);
    let pending = list.length;
    if (!pending) return done(null, results);

    list.forEach(function (file) {
      file = path.resolve(dir, file);

      fs.stat(file, function (err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function (err, res) {
            results = results.concat(res.filter(fileTest));
            if (!--pending) done(null, results);
          });
        } else {
          if (fileTest(file)) results.push(file);
          if (!--pending) done(null, results);
        }
      });
    });
  });
};

walk(libPath, function(err, results) {
  if (err) throw err;
  let indexFile = '';

  results.forEach(f => {
    const exp = f.replace(libPath, './lib').replace('.ts', '');
    indexFile += `export * from '${exp}';\n`;
  });

  fs.writeFile(path.join(__dirname, 'src', 'index.ts'), indexFile, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
});
