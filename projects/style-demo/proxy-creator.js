#!/usr/bin/env node
"use strict";

const fs = require('fs');
const path = require('path');

const proxyModule = name => `import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ${name}Module, ${name}Component } from '@ninjui/style-demo';

@NgModule({
  imports: [
    ${name}Module,
    RouterModule.forChild([
      { path: '', component: ${name}Component, pathMatch: 'full' }
    ])
  ]
})
export class Lazy${name}Module { }
`;

const regex = /\/([^/]+)\.component/mg;
const proxiesPath = path.join(process.cwd(), process.argv[2]);

fs.mkdir(path.join(process.cwd(), process.argv[2]), { recursive: true }, (err) => {
  if (err) throw err;

  fs.readFile(path.join(__dirname, 'index.d.ts'), 'utf8', (err, data) => {
    if (err) throw err;
    let m;

    while ((m = regex.exec(data)) !== null) {
      if (m.index === regex.lastIndex) regex.lastIndex++;
      let module = m[1].replace(/-([a-z])/g, g => g[1].toUpperCase()).replace(/^\w/, c => c.toUpperCase());
      // m[1].replace(/[-\.]([a-z])/g, g => g[1].toUpperCase()).replace(/^\w/, c => c.toUpperCase());

      fs.writeFile(path.join(proxiesPath, `lazy-${m[1]}.module.ts`), proxyModule(module), err => {
        console.log(err);
      });
    }
  });
});
