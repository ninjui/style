import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: '[demo-app]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  nav = [
    {
      text: 'Core Test',
      link: '/core'
    },
    {
      text: 'Typography',
      link: '/typography'
    },
    {
      text: 'Layout',
      link: '/layout',
      children: [
        {
          text: 'Container',
          link: '/layout/container'
        },
        {
          text: 'Cosmetic',
          link: '/layout/cosmetic'
        }
      ]
    },
    {
      text: 'Buttons',
      link: '/buttons'
    },
    {
      text: 'Form',
      link: '/forms',
      children: [
        {
          text: 'Container',
          link: '/forms/control-wrapper'
        },
      ]
    },
    {
      text: 'Utility',
      link: '/utilities',
      children: [
        {
          text: 'Display',
          link: '/utilities/display'
        },
        {
          text: 'Breakpoints',
          link: '/utilities/breakpoints'
        },
        {
          text: 'Flex',
          link: '/utilities/flex'
        }
      ]
    },
    {
      text: 'Components',
      link: '/components',
      children: [
        {
          text: 'Sidebar',
          link: '/components/sidebar'
        },
        {
          text: 'Tabs',
          link: '/components/tabs'
        }
      ]
    },
  ];

  private fragment: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.fragment.subscribe(fragment => {
      this.fragment = fragment;
      this.ngAfterViewInit();
    });
  }

  ngAfterViewInit(): void {
    if (this.fragment) {
      setTimeout(() => {
        document.querySelector('#' + this.fragment).scrollIntoView();
      });
    }
  }
}
