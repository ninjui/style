import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { styleDemoRoutes } from '@ninjui/style-demo';

const routes: Routes = styleDemoRoutes;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
