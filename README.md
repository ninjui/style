# Style

## Colors

https://www.tigercolor.com/color-lab/tips/tip-01.html

* Primary (10%)
  * CTAs and major actions
  * Stand out from other colors
  * Rarely used to promote importance
* Accent (30%)
  * Bring color to a screen
  * Smaller-area backgrounds
  * Less important info
* Filler (60%)
  * Larger-area Backgrounds
  * Color seen the most


**Primary** should be a bold and vibrant color and used for “eye catchers” while at the same time promoting your brand. 

Used to attract the users’ attention and prompt them to take action. Elements like:
* Call To Action buttons
* 

---

**Secondary** should be used for less important information, like:
* Secondary action buttons
* Active menu items
* Subheadings 
* Colored backgrounds
* Text content aimed at your business like FAQs or reviews.

---

**Neutrals** can be used to drive focus in colorful areas, such as:
* Text on secondary background
* Backgrounds that fill empty space

---

**Stateful/Contextual** colors are somewhat reserved as they're normally associated with a status:
* Reds for errors or 'dangerous' actions
* Yellows for warnings
* Greens for success or positive actions
* Light blue is normally associated with information

When choosing a primary/secondary color that falls within these pre-associated colors, be sure to either get enough difference between them to tell them apart or discover a replacement that stands out enough to be intuitive.