'use strict';

const gulp          = require('gulp'),
      pkg           = require('./projects/style/package.json'),
      transfob      = require('transfob'),
      del           = require('del'),
      sass          = require('gulp-sass'),
      rename        = require('gulp-rename'),
      postcss       = require('gulp-postcss'),
      prefix        = require('autoprefixer'),
      minify        = require('cssnano'),
      config        = {
          src:         './projects/style',
          dist:        './dist/style',
          outputStyle: 'compressed',
          srcGlob:     '/**/*.scss',
          mapsDir:     './maps',
      };

const banner = `/*!
 *  ${pkg.name} v${pkg.version} - ${pkg.homepage}
 *  (c) ${new Date().getFullYear()} ${pkg.author.name}
 *  License - https://gitlab.com/ninjui/style/blob/master/LICENSING.md
 */\n`;

gulp.task('clean', () => {
    return del([config.dist]);
});

gulp.task('copy', () => {
    return gulp.src(config.src + config.srcGlob)
    .pipe(gulp.dest(config.dist + '/scss'));
});

gulp.task('build', () => {
    return gulp.src(config.src + config.srcGlob)
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: true
        }))
        .pipe(postcss([
            prefix({
                cascade: true,
                remove: true
            })
        ]))
        .pipe(transfob(function(file, enc, next) {
            file.contents = Buffer.concat([Buffer.from(banner), file.contents]);
            this.push(file);
            next();
        }))
        .pipe(gulp.dest(config.dist + '/css'))
		.pipe(postcss([
			minify({
				discardComments: {
					removeAll: true
				}
			})
		]))
        .pipe(gulp.dest(config.dist + '/min'));
});

gulp.task('clean-build', gulp.series('clean', 'copy', 'build'));
gulp.task('default', gulp.series('clean-build'));

gulp.task('watch', gulp.series('clean', 'build', function watcher() {
    return gulp.watch(config.src + config.srcGlob, gulp.series('build'));
}));
