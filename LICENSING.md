NinjUI software is licensed under [AGPL](LICENSE) by default but a [MIT](COMM-LICENSE) is available for purchase.

NinjUI is availble for use in opensource software but for use in commercial/privately distributed software, a MIT license is required.
